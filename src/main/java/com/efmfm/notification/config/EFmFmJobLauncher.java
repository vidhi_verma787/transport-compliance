package com.efmfm.notification.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.efmfm.notification.control.EFmFmNotificationManager;

@Component
public class EFmFmJobLauncher {
	
	@Autowired
	EFmFmNotificationManager notificationManager;
	
	private static final Logger log = LoggerFactory.getLogger(EFmFmJobLauncher.class);

    @Scheduled(cron ="${efmfm.cron}")
    public void executeJob() throws IOException {
       log.info("Job Exceution started....");
       notificationManager.triggerNotificationJob();
       log.info("Job Exceution End....");
    }

	
	
	
}
