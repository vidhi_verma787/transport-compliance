package com.efmfm.notification.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="efmfm")
public class EFmFmPropertiesConfig {
	
	private String lastExecution;
	private String complianceBranchConfig;
	private String driverDocumentsValid;
	private String vehicleDocumentsValid;
	private String vehicleStateValid;
	private String vehicleNationalValid;
	private String vehiclePollutionValid;
	private String vehicleInsuranceValid;
	private String vehicleTaxValid;
	private String vehicleFitnessValid;
	private String lastExecutionVendor;
	private String vendorCompConfigBranch;
	private String uniqueVendor;
	private String vendorLicenseValid;
	private String vendorPoliceValid;
	private String vendorMedicalFitnessValid;
	private String vendorDdTraningValid;
	private String vendorStatePermitValid;
	private String vendorNationalPermitValid;
	private String vendorPollutionValid;
	private String vendorInsuranceValid;
	private String vendorTaxValid;
	private String vendorFitnessValid;
	private String smsUrl;
	private String transportVendorEmail;
	private String transportVendorSms;
	private String transportDriverSms;
	private String transportVehicleSms;
	private String transportDriverEmail;
	private String transportVehicleEmail;
	private String transportRegardsSms;
	private String transportRegardsEmail;
	private String emailSubject;
	private String emailDefaultMail;
	private String combinedFacility;
	private String uploadDocsLinux;
	private String uploadDocsWindows;
	private String emailDetails;
	
	public String getLastExecution() {
		return lastExecution;
	}

	public void setLastExecution(String lastExecution) {
		this.lastExecution = lastExecution;
	}

	public String getDriverPoliceValid() {
		return driverDocumentsValid;
	}

	public void setDriverPoliceValid(String driverPoliceValid) {
		this.driverDocumentsValid = driverPoliceValid;
	}

	public String getComplianceBranchConfig() {
		return complianceBranchConfig;
	}

	public void setComplianceBranchConfig(String complianceBranchConfig) {
		this.complianceBranchConfig = complianceBranchConfig;
	}	

	public String getDriverDocumentsValid() {
		return driverDocumentsValid;
	}

	public void setDriverDocumentsValid(String driverDocumentsValid) {
		this.driverDocumentsValid = driverDocumentsValid;
	}

	public String getVehicleStateValid() {
		return vehicleStateValid;
	}

	public void setVehicleStateValid(String vehicleStateValid) {
		this.vehicleStateValid = vehicleStateValid;
	}

	public String getVehicleNationalValid() {
		return vehicleNationalValid;
	}

	public void setVehicleNationalValid(String vehicleNationalValid) {
		this.vehicleNationalValid = vehicleNationalValid;
	}

	public String getVehiclePollutionValid() {
		return vehiclePollutionValid;
	}

	public void setVehiclePollutionValid(String vehiclePollutionValid) {
		this.vehiclePollutionValid = vehiclePollutionValid;
	}

	public String getVehicleInsuranceValid() {
		return vehicleInsuranceValid;
	}

	public void setVehicleInsuranceValid(String vehicleInsuranceValid) {
		this.vehicleInsuranceValid = vehicleInsuranceValid;
	}

	public String getVehicleTaxValid() {
		return vehicleTaxValid;
	}

	public void setVehicleTaxValid(String vehicleTaxValid) {
		this.vehicleTaxValid = vehicleTaxValid;
	}

	public String getVehicleFitnessValid() {
		return vehicleFitnessValid;
	}

	public void setVehicleFitnessValid(String vehicleFitnessValid) {
		this.vehicleFitnessValid = vehicleFitnessValid;
	}

	public String getLastExecutionVendor() {
		return lastExecutionVendor;
	}

	public void setLastExecutionVendor(String lastExecutionVendor) {
		this.lastExecutionVendor = lastExecutionVendor;
	}

	public String getVendorCompConfigBranch() {
		return vendorCompConfigBranch;
	}

	public void setVendorCompConfigBranch(String vendorCompConfigBranch) {
		this.vendorCompConfigBranch = vendorCompConfigBranch;
	}

	public String getUniqueVendor() {
		return uniqueVendor;
	}

	public void setUniqueVendor(String uniqueVendor) {
		this.uniqueVendor = uniqueVendor;
	}

	public String getVendorLicenseValid() {
		return vendorLicenseValid;
	}

	public void setVendorLicenseValid(String vendorLicenseValid) {
		this.vendorLicenseValid = vendorLicenseValid;
	}

	public String getVendorPoliceValid() {
		return vendorPoliceValid;
	}

	public void setVendorPoliceValid(String vendorPoliceValid) {
		this.vendorPoliceValid = vendorPoliceValid;
	}

	public String getVendorMedicalFitnessValid() {
		return vendorMedicalFitnessValid;
	}

	public void setVendorMedicalFitnessValid(String vendorMedicalFitnessValid) {
		this.vendorMedicalFitnessValid = vendorMedicalFitnessValid;
	}

	public String getVendorDdTraningValid() {
		return vendorDdTraningValid;
	}

	public void setVendorDdTraningValid(String vendorDdTraningValid) {
		this.vendorDdTraningValid = vendorDdTraningValid;
	}

	public String getVendorStatePermitValid() {
		return vendorStatePermitValid;
	}

	public void setVendorStatePermitValid(String vendorStatePermitValid) {
		this.vendorStatePermitValid = vendorStatePermitValid;
	}

	public String getVendorNationalPermitValid() {
		return vendorNationalPermitValid;
	}

	public void setVendorNationalPermitValid(String vendorNationalPermitValid) {
		this.vendorNationalPermitValid = vendorNationalPermitValid;
	}

	public String getVendorPollutionValid() {
		return vendorPollutionValid;
	}

	public void setVendorPollutionValid(String vendorPollutionValid) {
		this.vendorPollutionValid = vendorPollutionValid;
	}

	public String getVendorInsuranceValid() {
		return vendorInsuranceValid;
	}

	public void setVendorInsuranceValid(String vendorInsuranceValid) {
		this.vendorInsuranceValid = vendorInsuranceValid;
	}

	public String getVendorTaxValid() {
		return vendorTaxValid;
	}

	public void setVendorTaxValid(String vendorTaxValid) {
		this.vendorTaxValid = vendorTaxValid;
	}

	public String getVendorFitnessValid() {
		return vendorFitnessValid;
	}

	public void setVendorFitnessValid(String vendorFitnessValid) {
		this.vendorFitnessValid = vendorFitnessValid;
	}

	public String getSmsUrl() {
		return smsUrl;
	}

	public void setSmsUrl(String smsUrl) {
		this.smsUrl = smsUrl;
	}

	public String getTransportVendorEmail() {
		return transportVendorEmail;
	}

	public void setTransportVendorEmail(String transportVendorEmail) {
		this.transportVendorEmail = transportVendorEmail;
	}

	public String getTransportVendorSms() {
		return transportVendorSms;
	}

	public void setTransportVendorSms(String transportVendorSms) {
		this.transportVendorSms = transportVendorSms;
	}

	public String getTransportRegardsSms() {
		return transportRegardsSms;
	}

	public void setTransportRegardsSms(String transportRegardsSms) {
		this.transportRegardsSms = transportRegardsSms;
	}

	public String getTransportRegardsEmail() {
		return transportRegardsEmail;
	}

	public void setTransportRegardsEmail(String transportRegardsEmail) {
		this.transportRegardsEmail = transportRegardsEmail;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailDefaultMail() {
		return emailDefaultMail;
	}

	public void setEmailDefaultMail(String emailDefaultMail) {
		this.emailDefaultMail = emailDefaultMail;
	}

	public String getCombinedFacility() {
		return combinedFacility;
	}

	public void setCombinedFacility(String combinedFacility) {
		this.combinedFacility = combinedFacility;
	}

	public String getTransportDriverSms() {
		return transportDriverSms;
	}

	public void setTransportDriverSms(String transportDriverSms) {
		this.transportDriverSms = transportDriverSms;
	}

	public String getTransportVehicleSms() {
		return transportVehicleSms;
	}

	public void setTransportVehicleSms(String transportVehicleSms) {
		this.transportVehicleSms = transportVehicleSms;
	}

	public String getTransportDriverEmail() {
		return transportDriverEmail;
	}

	public void setTransportDriverEmail(String transportDriverEmail) {
		this.transportDriverEmail = transportDriverEmail;
	}

	public String getTransportVehicleEmail() {
		return transportVehicleEmail;
	}

	public void setTransportVehicleEmail(String transportVehicleEmail) {
		this.transportVehicleEmail = transportVehicleEmail;
	}

	public String getUploadDocsLinux() {
		return uploadDocsLinux;
	}

	public void setUploadDocsLinux(String uploadDocsLinux) {
		this.uploadDocsLinux = uploadDocsLinux;
	}

	public String getUploadDocsWindows() {
		return uploadDocsWindows;
	}

	public void setUploadDocsWindows(String uploadDocsWindows) {
		this.uploadDocsWindows = uploadDocsWindows;
	}

	public String getEmailDetails() {
		return emailDetails;
	}

	public void setEmailDetails(String emailDetails) {
		this.emailDetails = emailDetails;
	}

	public String getVehicleDocumentsValid() {
		return vehicleDocumentsValid;
	}

	public void setVehicleDocumentsValid(String vehicleDocumentsValid) {
		this.vehicleDocumentsValid = vehicleDocumentsValid;
	}

	@Override
	public String toString() {
		return "EFmFmPropertiesConfig [lastExecution=" + lastExecution + ", complianceBranchConfig="
				+ complianceBranchConfig + ", driverDocumentsValid=" + driverDocumentsValid + ", vehicleDocumentsValid="
				+ vehicleDocumentsValid + ", vehicleStateValid=" + vehicleStateValid + ", vehicleNationalValid="
				+ vehicleNationalValid + ", vehiclePollutionValid=" + vehiclePollutionValid + ", vehicleInsuranceValid="
				+ vehicleInsuranceValid + ", vehicleTaxValid=" + vehicleTaxValid + ", vehicleFitnessValid="
				+ vehicleFitnessValid + ", lastExecutionVendor=" + lastExecutionVendor + ", vendorCompConfigBranch="
				+ vendorCompConfigBranch + ", uniqueVendor=" + uniqueVendor + ", vendorLicenseValid="
				+ vendorLicenseValid + ", vendorPoliceValid=" + vendorPoliceValid + ", vendorMedicalFitnessValid="
				+ vendorMedicalFitnessValid + ", vendorDdTraningValid=" + vendorDdTraningValid
				+ ", vendorStatePermitValid=" + vendorStatePermitValid + ", vendorNationalPermitValid="
				+ vendorNationalPermitValid + ", vendorPollutionValid=" + vendorPollutionValid
				+ ", vendorInsuranceValid=" + vendorInsuranceValid + ", vendorTaxValid=" + vendorTaxValid
				+ ", vendorFitnessValid=" + vendorFitnessValid + ", smsUrl=" + smsUrl + ", transportVendorEmail="
				+ transportVendorEmail + ", transportVendorSms=" + transportVendorSms + ", transportDriverSms="
				+ transportDriverSms + ", transportVehicleSms=" + transportVehicleSms + ", transportDriverEmail="
				+ transportDriverEmail + ", transportVehicleEmail=" + transportVehicleEmail + ", transportRegardsSms="
				+ transportRegardsSms + ", transportRegardsEmail=" + transportRegardsEmail + ", emailSubject="
				+ emailSubject + ", emailDefaultMail=" + emailDefaultMail + ", combinedFacility=" + combinedFacility
				+ ", uploadDocsLinux=" + uploadDocsLinux + ", uploadDocsWindows=" + uploadDocsWindows
				+ ", emailDetails=" + emailDetails + "]";
	}

	
}
