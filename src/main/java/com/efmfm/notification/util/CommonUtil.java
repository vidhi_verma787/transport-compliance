package com.efmfm.notification.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class CommonUtil {

	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	public static boolean isTriggerNotification(List<Object[]> lastExecutionList, int repeatAlert, Date currentDate) {
		Object[] lastExecution = null;
		if (!lastExecutionList.isEmpty())
			lastExecution = lastExecutionList.get(0);
		if (lastExecution != null && lastExecution[0] != null) {
			Date lastExecutionDate = (Date) lastExecution[0];
			Date nextExecutionDate = CommonUtil.incrOrDecDaysByGivenDate(lastExecutionDate, repeatAlert);
			if (CommonUtil.getDatewithOutHHMMMMSS(currentDate)
					.compareTo(CommonUtil.getDatewithOutHHMMMMSS(nextExecutionDate)) == 0
					|| CommonUtil.getDatewithOutHHMMMMSS(nextExecutionDate)
							.compareTo(CommonUtil.getDatewithOutHHMMMMSS(currentDate)) < 0) {
				return true;
			}
		}
		return false;
	}

	public static int getcomplianceStatusForNotifyType(Date expiryDate, Date currentDate, int expiryDays[],
			int complianceType, int notifyType) {
		int status = 1;

		Date beforeExpiryDate[] = new Date[10];

		if (complianceType == ComplianceType.DRIVER.ordinal()) {
			beforeExpiryDate[0] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[0]);
			beforeExpiryDate[1] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[1]);
			beforeExpiryDate[2] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[2]);
			beforeExpiryDate[3] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[3]);

			if (notifyType == NotificationType.LICENSE_EXPIRE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[0]);
			} else if (notifyType == NotificationType.MEDICAL_FITNESS.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[1]);
			} else if (notifyType == NotificationType.POLICE_VERIFICATION.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[2]);
			} else if (notifyType == NotificationType.DD_TRAINING.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[3]);
			}

		} else {

			beforeExpiryDate[0] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[0]);
			beforeExpiryDate[1] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[1]);
			beforeExpiryDate[2] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[2]);
			beforeExpiryDate[3] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[3]);
			beforeExpiryDate[4] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[4]);
			beforeExpiryDate[5] = CommonUtil.getAdditionDate(expiryDate, -expiryDays[5]);

			if (notifyType == NotificationType.POLLUTION_DUE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[0]);
			} else if (notifyType == NotificationType.INSURANCE_DUE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[1]);
			} else if (notifyType == NotificationType.TAX_DUE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[2]);
			} else if (notifyType == NotificationType.STATE_PERMIT_DUE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[3]);
			} else if (notifyType == NotificationType.NATIONAL_PERMIT_DUE.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[4]);
			} else if (notifyType == NotificationType.VEHICLE_FITNESS.ordinal()) {
				status = getcomplianceStatus(expiryDate, currentDate, beforeExpiryDate[5]);
			}
		}
		return status;
	}

	public static int getcomplianceStatus(Date expiryDate, Date currentDate, Date beforeExpiryDate) {
		if (CommonUtil.getDatewithOutHHMMMMSS(currentDate)
				.compareTo(CommonUtil.getDatewithOutHHMMMMSS(expiryDate)) >= 0)
			return complianceStatus.NOT_COMPLIANT.ordinal();
		else if (CommonUtil.getDatewithOutHHMMMMSS(currentDate)
				.compareTo(CommonUtil.getDatewithOutHHMMMMSS(beforeExpiryDate)) >= 0)
			return complianceStatus.APPROACHING_TO_NONCOMPLIANT.ordinal();
		else
			return complianceStatus.COMPLIANT.ordinal();

	}

	public static List<String> getSplitedList(String str) {
		return Arrays.asList(str.split("\\s*,\\s*"));
	}

	public static Date getZoneDateTime(String timeZone) {
		Date today = new Date();
		Date zoneDate = null;
		DateFormat df = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		String zoneDateTime = df.format(today);
		try {
			zoneDate = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(zoneDateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return zoneDate;
	}

	public static Date incrOrDecDaysByGivenDate(Date givenDate, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(givenDate);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}

	public static Date getDatewithOutHHMMMMSS(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	public static Date getAdditionDate(Date timeZoneDateTime, int days) {
		GregorianCalendar greCalendor = new GregorianCalendar();
		greCalendor.setTime(timeZoneDateTime);
		greCalendor.add(Calendar.DATE, +days);
		return greCalendor.getTime();

	}

	public static Date getSubstractedDate(Date timeZoneDateTime, int days) {
		GregorianCalendar greCalendor = new GregorianCalendar();
		greCalendor.setTime(timeZoneDateTime);
		greCalendor.add(Calendar.DATE, -days);
		return greCalendor.getTime();

	}

	public enum AuditType {
		INTERNAL, VENDOR
	}

	public enum ComplianceType {
		DRIVER, VEHICLE, VENDOR
	}

	public enum complianceStatus {
		APPROACHING_TO_NONCOMPLIANT, COMPLIANT, NOT_COMPLIANT
	}

	public enum NotificationType {
		LICENSE_EXPIRE("driver", "License"), MEDICAL_FITNESS("driver", "Medical Fitness"),
		POLICE_VERIFICATION("driver", "Police Verification"), DD_TRAINING("driver", "DD Training"),
		STATE_PERMIT_DUE("vehicle", "State Permit Due"), NATIONAL_PERMIT_DUE("vehicle", "NationalPermit Due"),
		POLLUTION_DUE("vehicle", "Pollution Due"), INSURANCE_DUE("vehicle", "Insurance Due"),
		TAX_DUE("vehicle", "Tax Due"), VEHICLE_FITNESS("vehicle", "Vehicle Fitness");
		private String type;
		private String documentType;

		private NotificationType(String type, String documentType) {
			this.type = type;
			this.documentType = documentType;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDocumentType() {
			return documentType;
		}

		public void setDocumentType(String documentType) {
			this.documentType = documentType;
		}

	}

	public enum NotificationStatus {
		SENT, NOTSENT
	}

	public enum ContactType {
		SMS, EMAIL
	}

}
