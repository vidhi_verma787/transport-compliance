package com.efmfm.notification.util;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Component;

import com.efmfm.notification.email.EmailDetails;
import com.efmfm.notification.email.GoDaddyEmailService;
import com.efmfm.notification.email.SendGridEmailService;
import com.efmfm.notification.model.EmailServiceConfig;

@Component
public class MailNotificationUtil {
	public void sendEmail1(String subject, String emailContent, File file, String sentBy, String emailId, String[] mailDetails, String transportEmailId) {

		// Sender's email ID needs to be mentioned
		final String username = mailDetails[0];// change accordingly
		final String password = mailDetails[1];// change accordingly
		
		Properties props = new Properties();
		props.put("mail.smtp.host", mailDetails[2]);
		props.put("mail.smtp.auth", mailDetails[3]);
		props.put("mail.smtp.starttls.enable", mailDetails[4]);
		props.put("mail.smtp.port", mailDetails[5]);
		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));	
			String[] mailRecipient = emailId.split(",");
			Address[] ia = new InternetAddress[mailRecipient.length];
			int i = 0;
			for (String address : mailRecipient) {
				ia[i] = new InternetAddress(address);
			    i++;
			}
			message.addRecipients(RecipientType.TO, ia);
			message.setSubject(subject);
			message.setSentDate(new Date());
			
			String[] transportEmailRecipient = transportEmailId.split(",");
			Address[] iaddress = new InternetAddress[transportEmailRecipient.length];
			int index = 0;
			for (String address : transportEmailRecipient) {
				iaddress[index] = new InternetAddress(address);
				index++;
			}
			
			if(transportEmailId!=null && !"".equals(transportEmailId))  {
				message.setRecipients(Message.RecipientType.CC,	iaddress); 
			}
			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(emailContent+"\n");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			if(file != null) {
				DataSource source = new FileDataSource(file.getAbsolutePath());
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(file.getName());
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				// Send message
				Transport.send(message);
			}
			
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	public void sendEmail(String subject, String emailContent, File file, String sentBy, String emailId, Object mailDetails, String transportEmailId) {
		
		Object[] mailDetail = (Object[])mailDetails;
		EmailDetails emailDetails=new EmailDetails();
        emailDetails.setUserName(mailDetail[0].toString());
        emailDetails.setPassword(mailDetail[1].toString());
        emailDetails.setHost(mailDetail[2].toString());
        emailDetails.setAuth(mailDetail[3].toString());
        emailDetails.setSecureSSLORTLS(mailDetail[4].toString());
        emailDetails.setPort(Integer.parseInt(mailDetail[5].toString()));
        emailDetails.setSendGridKey(mailDetail[6].toString());
        emailDetails.setBodyContent(emailContent);
        emailDetails.setFile(file);
        emailDetails.setFrom(mailDetail[0].toString());
        emailDetails.setTo(emailId);
        if(transportEmailId!=null && !"".equals(transportEmailId)) 
        	emailDetails.setCc(transportEmailId);
        emailDetails.setSubject(subject);
        
        try {
        	SendGridEmailService.sendMail(emailDetails);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
        
	}


}
