package com.efmfm.notification.control;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;

import com.efmfm.notification.config.EFmFmJobLauncher;
import com.efmfm.notification.config.EFmFmPropertiesConfig;
import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.model.EmailServiceConfig;
import com.efmfm.notification.service.EFmFmNotificationService;
import com.efmfm.notification.util.CommonUtil;
import com.efmfm.notification.util.CommonUtil.AuditType;
import com.efmfm.notification.util.CommonUtil.complianceStatus;
import com.efmfm.notification.util.CommonUtil.ComplianceType;
import com.efmfm.notification.util.CommonUtil.ContactType;
import com.efmfm.notification.util.CommonUtil.NotificationStatus;
import com.efmfm.notification.util.CommonUtil.NotificationType;
import com.efmfm.notification.util.MailNotificationUtil;
import com.efmfm.notification.util.SMSNotificationUtil;

@Component
public class EFmFmNotificationManager {
	private static final Logger log = LoggerFactory.getLogger(EFmFmJobLauncher.class);

	@Autowired
	EFmFmNotificationService notificationService;

	@Autowired
	MailNotificationUtil mailNotification;

	@Autowired
	EFmFmPropertiesConfig propertiesConfig;

	public void triggerNotificationJob() {
		List<Object[]> clientBranchList = notificationService.getAllBranchDetails(
				propertiesConfig.getComplianceBranchConfig(),
				CommonUtil.getSplitedList(propertiesConfig.getCombinedFacility()));
		executeDriverCompliance(clientBranchList);
		executeVehicleCompliance(clientBranchList);
		// executeVendorCompliance();
	}

	private void executeDriverCompliance(List<Object[]> clientBranchList) {
		int[] expiryDays = new int[5];
		for (Object[] clientBranch : clientBranchList) {
			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;

			expiryDays[0] = clientBranch[3] != null ? (int) clientBranch[3] : 0;
			int licenseRepeatAlertsDay = clientBranch[4] != null ? (int) clientBranch[4] : 0;
			String transportEmailId = clientBranch[5] != null ? ((String) clientBranch[5] + ",") : "";
			String licenseNotifiyType = clientBranch[6] != null ? (String) clientBranch[6] : "";
			String smsNumber = clientBranch[7] != null ? ((String) clientBranch[7] + ",") : "";

			expiryDays[1] = clientBranch[8] != null ? (int) clientBranch[8] : 0;
			// int medicalRepeatAlertsDay = clientBranch[9] != null ? (int) clientBranch[9]
			// : 0;
			if (clientBranch[10] != null) {
				if (transportEmailId.indexOf(clientBranch[10].toString().trim()) < 0)
					transportEmailId += clientBranch[10] != null ? ((String) clientBranch[10] + ",") : "";
			}

			String medicalNotifiyType = clientBranch[11] != null ? (String) clientBranch[11] : "";
			smsNumber += clientBranch[12] != null ? ((String) clientBranch[12] + ",") : "";
			expiryDays[2] = clientBranch[13] != null ? (int) clientBranch[13] : 0;
			// int policeVerifyRepeatAlertsDay = clientBranch[14] != null ? (int)
			// clientBranch[14] : 0;
			if (clientBranch[15] != null) {
				if (transportEmailId.indexOf(clientBranch[15].toString().trim()) < 0)
					transportEmailId += clientBranch[15] != null ? ((String) clientBranch[15] + ",") : "";
			}
			String policeVerifyNotifiyType = clientBranch[16] != null ? (String) clientBranch[16] : "";
			smsNumber += clientBranch[17] != null ? ((String) clientBranch[17] + ",") : "";

			expiryDays[3] = clientBranch[18] != null ? (int) clientBranch[18] : 0;
			// int ddTrainingRepeatAlertsDay = clientBranch[19] != null ? (int)
			// clientBranch[19] : 0;
			if (clientBranch[20] != null) {
				if (transportEmailId.indexOf(clientBranch[20].toString().trim()) < 0)
					transportEmailId += clientBranch[20] != null ? ((String) clientBranch[20] + ",") : "";
			}
			String ddTrainingNotifiyType = clientBranch[21] != null ? (String) clientBranch[21] : "";
			smsNumber += clientBranch[22] != null ? ((String) clientBranch[22] + ",") : "";

			String currentTimeZone = clientBranch[53] != null ? (String) clientBranch[53] : "";

			if (transportEmailId.charAt(transportEmailId.length() - 1) == ',')
				transportEmailId = transportEmailId.substring(0, transportEmailId.length() - 1);

			if (smsNumber.charAt(smsNumber.length() - 1) == ',')
				smsNumber = smsNumber.substring(0, smsNumber.length() - 1);

			List<Object[]> vendorList = notificationService
					.getVendorDetailsByBranchId(propertiesConfig.getUniqueVendor(), branchId);

			for (Object[] vendorDetail : vendorList) {
				String sqlQuery = propertiesConfig.getDriverDocumentsValid();
				if (currentTimeZone != null) {
					Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
					if (expiryDays[0] != 0 && !"NONE".equalsIgnoreCase(licenseNotifiyType)) {
						sqlQuery += " DATE(dm.LicenceValid) <= DATE(:licenseEndDate) OR";
					}
					if (expiryDays[1] != 0 && !"NONE".equalsIgnoreCase(medicalNotifiyType)) {
						sqlQuery += " DATE(dm.MedicalFitnessCertificateValid) <= DATE(:medicalEndDate) OR";
					}
					if (expiryDays[2] != 0 && !"NONE".equalsIgnoreCase(policeVerifyNotifiyType)) {
						sqlQuery += " DATE(dm.PoliceVarificationValid) <= DATE(:policeEndDate) OR";
					}
					if (expiryDays[3] != 0 && !"NONE".equalsIgnoreCase(ddTrainingNotifiyType)) {
						sqlQuery += " DATE(dm.DDTVALID) <= DATE(:ddtEndDate)";
					}

					if (sqlQuery.substring(sqlQuery.length() - 2, sqlQuery.length() - 1).equalsIgnoreCase("OR"))
						sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 3);

					sqlQuery += ")";
					processNotification(branchId, currentDate, expiryDays, licenseRepeatAlertsDay, transportEmailId,
							licenseNotifiyType, smsNumber, ComplianceType.DRIVER, null, sqlQuery, vendorDetail);
				}
			}

		}
	}

	private void executeVehicleCompliance(List<Object[]> clientBranchList) {
		int[] expiryDays = new int[7];
		for (Object[] clientBranch : clientBranchList) {
			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;

			expiryDays[0] = clientBranch[23] != null ? (int) clientBranch[23] : 0;
			int pollutionDueRepeatAlertsDay = clientBranch[24] != null ? (int) clientBranch[24] : 0;
			String transportEmailId = clientBranch[25] != null ? ((String) clientBranch[25] + ",") : "";
			String pollutionDueNotifiyType = clientBranch[26] != null ? (String) clientBranch[26] : "";
			String smsNumber = clientBranch[27] != null ? ((String) clientBranch[27] + ",") : "";

			expiryDays[1] = clientBranch[28] != null ? (int) clientBranch[28] : 0;
			if (clientBranch[30] != null) {
				if (transportEmailId.indexOf(clientBranch[30].toString().trim()) < 0)
					transportEmailId += clientBranch[30] != null ? ((String) clientBranch[30] + ",") : "";
			}
			smsNumber += clientBranch[12] != null ? ((String) clientBranch[12] + ",") : "";
			String insuranceDueNotifiyType = clientBranch[31] != null ? (String) clientBranch[31] : "";
			smsNumber += clientBranch[32] != null ? ((String) clientBranch[32] + ",") : "";

			expiryDays[2] = clientBranch[33] != null ? (int) clientBranch[33] : 0;
			if (clientBranch[35] != null) {
				if (transportEmailId.indexOf(clientBranch[35].toString().trim()) < 0)
					transportEmailId += clientBranch[35] != null ? ((String) clientBranch[35] + ",") : "";
			}
			String taxCertificateNotifiyType = clientBranch[36] != null ? (String) clientBranch[36] : "";
			smsNumber += clientBranch[37] != null ? ((String) clientBranch[37] + ",") : "";

			expiryDays[3] = clientBranch[38] != null ? (int) clientBranch[38] : 0;
			if (clientBranch[40] != null) {
				if (transportEmailId.indexOf(clientBranch[40].toString().trim()) < 0)
					transportEmailId += clientBranch[40] != null ? ((String) clientBranch[40] + ",") : "";
			}
			String statePermitDueNotifiyType = clientBranch[41] != null ? (String) clientBranch[41] : "";
			smsNumber += clientBranch[42] != null ? ((String) clientBranch[42] + ",") : "";

			expiryDays[4] = clientBranch[43] != null ? (int) clientBranch[43] : 0;
			if (clientBranch[45] != null) {
				if (transportEmailId.indexOf(clientBranch[45].toString().trim()) < 0)
					transportEmailId += clientBranch[45] != null ? ((String) clientBranch[45] + ",") : "";
			}
			String natnlPermitDueNotifiyType = clientBranch[46] != null ? (String) clientBranch[46] : "";
			smsNumber += clientBranch[47] != null ? ((String) clientBranch[47] + ",") : "";

			// Here vehicle maintenance will be considered as vehicle fitness at
			// client branchPO Table
			expiryDays[5] = clientBranch[48] != null ? (int) clientBranch[48] : 0;
			if (clientBranch[50] != null) {
				if (transportEmailId.indexOf(clientBranch[50].toString().trim()) < 0)
					transportEmailId += clientBranch[50] != null ? ((String) clientBranch[50] + ",") : "";
			}
			String vehicelMaintenanceNotifiyType = clientBranch[51] != null ? (String) clientBranch[51] : "";
			smsNumber += clientBranch[52] != null ? ((String) clientBranch[52] + ",") : "";

			String currentTimeZone = clientBranch[53] != null ? (String) clientBranch[53] : "";

			if (smsNumber.charAt(smsNumber.length() - 1) == ',')
				smsNumber = smsNumber.substring(0, smsNumber.length() - 1);

			if (transportEmailId.charAt(transportEmailId.length() - 1) == ',')
				transportEmailId = transportEmailId.substring(0, transportEmailId.length() - 1);

			List<Object[]> vendorList = notificationService
					.getVendorDetailsByBranchId(propertiesConfig.getUniqueVendor(), branchId);

			/*
			 * String[] mailDetails = new String[7]; mailDetails[0] = clientBranch[54] !=
			 * null ? clientBranch[54].toString() : ""; mailDetails[1] = clientBranch[55] !=
			 * null ? clientBranch[55].toString() : ""; mailDetails[2] = clientBranch[56] !=
			 * null ? clientBranch[56].toString() : ""; mailDetails[3] = clientBranch[57] !=
			 * null ? clientBranch[57].toString() : ""; mailDetails[4] = clientBranch[58] !=
			 * null ? clientBranch[58].toString() : ""; //ssl mailDetails[5] =
			 * clientBranch[59] != null ? clientBranch[59].toString() : ""; //port
			 * 
			 * if (currentTimeZone != null) { Date currentDate =
			 * CommonUtil.getZoneDateTime(currentTimeZone); if (pollutionDueExpiryDay != 0
			 * && !"NONE".equalsIgnoreCase(pollutionDueNotifiyType)) {
			 * processNotification(branchId, currentDate, pollutionDueExpiryDay,
			 * pollutionDueRepeatAlertsDay, pollutionDueEmailId, pollutionDueNotifiyType,
			 * pollutionDueSMSNumber, ComplianceType.VEHICLE,
			 * NotificationType.POLLUTION_DUE, propertiesConfig.getVehiclePollutionValid(),
			 * null, mailDetails); } if (isuranceDueExpiryDay != 0 &&
			 * !"NONE".equalsIgnoreCase(insuranceDueNotifiyType)) {
			 * processNotification(branchId, currentDate, isuranceDueExpiryDay,
			 * insuranceDueRepeatAlertsDay, insuranceDueEmailId, insuranceDueNotifiyType,
			 * insuranceDueSMSNumber, ComplianceType.VEHICLE,
			 * NotificationType.INSURANCE_DUE, propertiesConfig.getVehicleInsuranceValid(),
			 * null, mailDetails); } if (taxCertificateExpiryDay != 0 &&
			 * !"NONE".equalsIgnoreCase(taxCertificateNotifiyType)) {
			 * processNotification(branchId, currentDate, taxCertificateExpiryDay,
			 * taxCertificateRepeatAlertsDay, taxCertificateEmailId,
			 * taxCertificateNotifiyType, taxCertificateSMSNumber, ComplianceType.VEHICLE,
			 * NotificationType.TAX_DUE, propertiesConfig.getVehicleTaxValid(), null,
			 * mailDetails); } if (statePermitDueExpiryDay != 0 &&
			 * !"NONE".equalsIgnoreCase(statePermitDueNotifiyType)) {
			 * processNotification(branchId, currentDate, statePermitDueExpiryDay,
			 * statePermitDueRepeatAlertsDay, statePermitDueEmailId,
			 * statePermitDueNotifiyType, statePermitDueSMSNumber, ComplianceType.VEHICLE,
			 * NotificationType.STATE_PERMIT_DUE, propertiesConfig.getVehicleStateValid(),
			 * null, mailDetails); } if (natnlPermitDueExpiryDay != 0 &&
			 * !"NONE".equalsIgnoreCase(natnlPermitDueNotifiyType)) {
			 * processNotification(branchId, currentDate, natnlPermitDueExpiryDay,
			 * natnlPermitDueRepeatAlertsDay, natnlPermitDueEmailId,
			 * natnlPermitDueNotifiyType, natnlPermitDueSMSNumber, ComplianceType.VEHICLE,
			 * NotificationType.NATIONAL_PERMIT_DUE,
			 * propertiesConfig.getVehicleNationalValid(), null, mailDetails); } if
			 * (vehicelMaintenanceExpiryDay != 0 &&
			 * !"NONE".equalsIgnoreCase(vehicelMaintenanceNotifiyType)) {
			 * processNotification(branchId, currentDate, vehicelMaintenanceExpiryDay,
			 * vehicelMaintenanceRepeatAlertsDay, vehicelMaintenanceEmailId,
			 * vehicelMaintenanceNotifiyType, vehicelMaintenanceSMSNumber,
			 * ComplianceType.VEHICLE, NotificationType.VEHICLE_FITNESS,
			 * propertiesConfig.getVehicleFitnessValid(), null, mailDetails); } }
			 */

			for (Object[] vendorDetail : vendorList) {
				String sqlQuery = propertiesConfig.getVehicleDocumentsValid();
				if (currentTimeZone != null) {
					Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
					if (expiryDays[0] != 0 && !"NONE".equalsIgnoreCase(pollutionDueNotifiyType)) {
						sqlQuery += " DATE(vm.PolutionValid) <=  DATE(:PolutionValid) OR";
					}
					if (expiryDays[1] != 0 && !"NONE".equalsIgnoreCase(insuranceDueNotifiyType)) {
						sqlQuery += " DATE(vm.InsuranceValidDate)<= DATE(:InsuranceValidDate) OR";
					}
					if (expiryDays[2] != 0 && !"NONE".equalsIgnoreCase(taxCertificateNotifiyType)) {
						sqlQuery += " DATE(vm.TaxCertificateValid) <= DATE(:TaxCertificateValid) OR";
					}
					if (expiryDays[3] != 0 && !"NONE".equalsIgnoreCase(statePermitDueNotifiyType)) {
						sqlQuery += " DATE(vm.StatePermit)<= DATE(:StatePermit) OR";
					}
					if (expiryDays[4] != 0 && !"NONE".equalsIgnoreCase(natnlPermitDueNotifiyType)) {
						sqlQuery += " DATE(vm.NationalPermit) <=  DATE(:NationalPermit) OR";
					}
					if (expiryDays[5] != 0 && !"NONE".equalsIgnoreCase(vehicelMaintenanceNotifiyType)) {
						sqlQuery += " DATE(vm.VehicleFitNessDate)<= DATE(:VehicleFitNessDate)";
					}

					if (sqlQuery.substring(sqlQuery.length() - 2, sqlQuery.length() - 1).equalsIgnoreCase("OR"))
						sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 3);

					sqlQuery += ")";
					processNotification(branchId, currentDate, expiryDays, pollutionDueRepeatAlertsDay,
							transportEmailId, pollutionDueNotifiyType, smsNumber, ComplianceType.VEHICLE, null,
							sqlQuery, vendorDetail);
				}
			}

		}

	}
//
//	private void executeVendorCompliance() {
//		List<Object[]> clientBranchList = notificationService.getAllBranchDetails(
//				propertiesConfig.getVendorCompConfigBranch(),
//				CommonUtil.getSplitedList(propertiesConfig.getCombinedFacility()));
//		for (Object[] clientBranch : clientBranchList) {
//			int branchId = clientBranch[0] != null ? (int) clientBranch[0] : 0;
//
//			int licenseExpiryDay = clientBranch[1] != null ? (int) clientBranch[1] : 0;
//			int licenseRepeatAlertsDay = clientBranch[2] != null ? (int) clientBranch[2] : 0;
//			String licenseEmailId = clientBranch[3] != null ? (String) clientBranch[3] : "";
//			String licenseNotifiyType = clientBranch[4] != null ? (String) clientBranch[4] : "";
//			String licenseSMSNumber = clientBranch[5] != null ? (String) clientBranch[5] : "";
//
//			int medicalExpiryDay = clientBranch[6] != null ? (int) clientBranch[6] : 0;
//			int medicalRepeatAlertsDay = clientBranch[7] != null ? (int) clientBranch[7] : 0;
//			String medicalEmailId = clientBranch[8] != null ? (String) clientBranch[8] : "";
//			String medicalNotifiyType = clientBranch[9] != null ? (String) clientBranch[9] : "";
//			String medicalSMSNumber = clientBranch[10] != null ? (String) clientBranch[10] : "";
//
//			int policeVerifyExpiryDay = clientBranch[11] != null ? (int) clientBranch[11] : 0;
//			int policeVerifyRepeatAlertsDay = clientBranch[12] != null ? (int) clientBranch[12] : 0;
//			String policeVerifyEmailId = clientBranch[13] != null ? (String) clientBranch[13] : "";
//			String policeVerifyNotifiyType = clientBranch[14] != null ? (String) clientBranch[14] : "";
//			String policeVerifySMSNumber = clientBranch[15] != null ? (String) clientBranch[15] : "";
//
//			int ddTrainingExpiryDay = clientBranch[16] != null ? (int) clientBranch[16] : 0;
//			int ddTrainingRepeatAlertsDay = clientBranch[17] != null ? (int) clientBranch[17] : 0;
//			String ddTrainingEmailId = clientBranch[18] != null ? (String) clientBranch[18] : "";
//			String ddTrainingNotifiyType = clientBranch[19] != null ? (String) clientBranch[19] : "";
//			String ddTrainingSMSNumber = clientBranch[20] != null ? (String) clientBranch[20] : "";
//
//			int pollutionDueExpiryDay = clientBranch[21] != null ? (int) clientBranch[21] : 0;
//			int pollutionDueRepeatAlertsDay = clientBranch[22] != null ? (int) clientBranch[22] : 0;
//			String pollutionDueEmailId = clientBranch[23] != null ? (String) clientBranch[23] : "";
//			String pollutionDueNotifiyType = clientBranch[24] != null ? (String) clientBranch[24] : "";
//			String pollutionDueSMSNumber = clientBranch[25] != null ? (String) clientBranch[25] : "";
//
//			int isuranceDueExpiryDay = clientBranch[26] != null ? (int) clientBranch[26] : 0;
//			int insuranceDueRepeatAlertsDay = clientBranch[27] != null ? (int) clientBranch[27] : 0;
//			String insuranceDueEmailId = clientBranch[28] != null ? (String) clientBranch[28] : "";
//			String insuranceDueNotifiyType = clientBranch[29] != null ? (String) clientBranch[29] : "";
//			String insuranceDueSMSNumber = clientBranch[30] != null ? (String) clientBranch[30] : "";
//
//			int taxCertificateExpiryDay = clientBranch[31] != null ? (int) clientBranch[31] : 0;
//			int taxCertificateRepeatAlertsDay = clientBranch[32] != null ? (int) clientBranch[32] : 0;
//			String taxCertificateEmailId = clientBranch[33] != null ? (String) clientBranch[35] : "";
//			String taxCertificateNotifiyType = clientBranch[34] != null ? (String) clientBranch[34] : "";
//			String taxCertificateSMSNumber = clientBranch[35] != null ? (String) clientBranch[35] : "";
//
//			int statePermitDueExpiryDay = clientBranch[36] != null ? (int) clientBranch[36] : 0;
//			int statePermitDueRepeatAlertsDay = clientBranch[37] != null ? (int) clientBranch[37] : 0;
//			String statePermitDueEmailId = clientBranch[38] != null ? (String) clientBranch[38] : "";
//			String statePermitDueNotifiyType = clientBranch[39] != null ? (String) clientBranch[39] : "";
//			String statePermitDueSMSNumber = clientBranch[40] != null ? (String) clientBranch[40] : "";
//
//			int natnlPermitDueExpiryDay = clientBranch[41] != null ? (int) clientBranch[41] : 0;
//			int natnlPermitDueRepeatAlertsDay = clientBranch[42] != null ? (int) clientBranch[42] : 0;
//			String natnlPermitDueEmailId = clientBranch[43] != null ? (String) clientBranch[43] : "";
//			String natnlPermitDueNotifiyType = clientBranch[44] != null ? (String) clientBranch[44] : "";
//			String natnlPermitDueSMSNumber = clientBranch[45] != null ? (String) clientBranch[45] : "";
//
//			// Here vehicle maintenance will be considered as vehicle fitness at
//			// client branchPO Table
//			int vehicelFitnessExpiryDay = clientBranch[46] != null ? (int) clientBranch[46] : 0;
//			int vehicelFitnessRepeatAlertsDay = clientBranch[47] != null ? (int) clientBranch[47] : 0;
//			String vehicelFitnessEmailId = clientBranch[48] != null ? (String) clientBranch[48] : "";
//			String vehicelFitnessNotifiyType = clientBranch[49] != null ? (String) clientBranch[49] : "";
//			String vehicelFitnessSMSNumber = clientBranch[50] != null ? (String) clientBranch[50] : "";
//
//			String currentTimeZone = clientBranch[51] != null ? (String) clientBranch[51] : "";
//
//			String[] mailDetails = new String[7];
//			mailDetails[0] = clientBranch[52] != null ? clientBranch[52].toString() : "";
//			mailDetails[1] = clientBranch[53] != null ? clientBranch[53].toString() : "";
//			mailDetails[2] = clientBranch[54] != null ? clientBranch[54].toString() : "";
//			mailDetails[3] = clientBranch[55] != null ? clientBranch[55].toString() : "";
//			mailDetails[4] = clientBranch[56] != null ? clientBranch[56].toString() : "";// ssl
//			mailDetails[5] = clientBranch[57] != null ? clientBranch[57].toString() : "";// port
//
//			List<Object[]> vendorList = notificationService
//					.getVendorDetailsByBranchId(propertiesConfig.getUniqueVendor(), branchId);
//			for (Object[] vendorDetail : vendorList) {
//				if (currentTimeZone != null) {
//					log.info("VENDOR NAME: " + vendorDetail[1]);
//					Date currentDate = CommonUtil.getZoneDateTime(currentTimeZone);
//					if (licenseExpiryDay != 0 && !"NONE".equalsIgnoreCase(licenseNotifiyType)) {
//						processNotification(branchId, currentDate, licenseExpiryDay, licenseRepeatAlertsDay,
//								licenseEmailId, licenseNotifiyType, licenseSMSNumber, ComplianceType.VENDOR,
//								NotificationType.LICENSE_EXPIRE, propertiesConfig.getVendorLicenseValid(), vendorDetail,
//								mailDetails);
//					}
//					if (medicalExpiryDay != 0 && !"NONE".equalsIgnoreCase(medicalNotifiyType)) {
//						processNotification(branchId, currentDate, medicalExpiryDay, medicalRepeatAlertsDay,
//								medicalEmailId, medicalNotifiyType, medicalSMSNumber, ComplianceType.VENDOR,
//								NotificationType.MEDICAL_FITNESS, propertiesConfig.getVendorMedicalFitnessValid(),
//								vendorDetail, mailDetails);
//					}
//					if (policeVerifyExpiryDay != 0 && !"NONE".equalsIgnoreCase(policeVerifyNotifiyType)) {
//						processNotification(branchId, currentDate, policeVerifyExpiryDay, policeVerifyRepeatAlertsDay,
//								policeVerifyEmailId, policeVerifyNotifiyType, policeVerifySMSNumber,
//								ComplianceType.VENDOR, NotificationType.POLICE_VERIFICATION,
//								propertiesConfig.getVendorPoliceValid(), vendorDetail, mailDetails);
//					}
//					if (ddTrainingExpiryDay != 0 && !"NONE".equalsIgnoreCase(ddTrainingNotifiyType)) {
//						processNotification(branchId, currentDate, ddTrainingExpiryDay, ddTrainingRepeatAlertsDay,
//								ddTrainingEmailId, ddTrainingNotifiyType, ddTrainingSMSNumber, ComplianceType.VENDOR,
//								NotificationType.DD_TRAINING, propertiesConfig.getVendorDdTraningValid(), vendorDetail,
//								mailDetails);
//					}
//					if (pollutionDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(pollutionDueNotifiyType)) {
//						processNotification(branchId, currentDate, pollutionDueExpiryDay, pollutionDueRepeatAlertsDay,
//								pollutionDueEmailId, pollutionDueNotifiyType, pollutionDueSMSNumber,
//								ComplianceType.VENDOR, NotificationType.POLLUTION_DUE,
//								propertiesConfig.getVendorPollutionValid(), vendorDetail, mailDetails);
//					}
//					if (isuranceDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(insuranceDueNotifiyType)) {
//						processNotification(branchId, currentDate, isuranceDueExpiryDay, insuranceDueRepeatAlertsDay,
//								insuranceDueEmailId, insuranceDueNotifiyType, insuranceDueSMSNumber,
//								ComplianceType.VENDOR, NotificationType.INSURANCE_DUE,
//								propertiesConfig.getVendorInsuranceValid(), vendorDetail, mailDetails);
//					}
//					if (taxCertificateExpiryDay != 0 && !"NONE".equalsIgnoreCase(taxCertificateNotifiyType)) {
//						processNotification(branchId, currentDate, taxCertificateExpiryDay,
//								taxCertificateRepeatAlertsDay, taxCertificateEmailId, taxCertificateNotifiyType,
//								taxCertificateSMSNumber, ComplianceType.VENDOR, NotificationType.TAX_DUE,
//								propertiesConfig.getVendorTaxValid(), vendorDetail, mailDetails);
//					}
//					if (statePermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(statePermitDueNotifiyType)) {
//						processNotification(branchId, currentDate, statePermitDueExpiryDay,
//								statePermitDueRepeatAlertsDay, statePermitDueEmailId, statePermitDueNotifiyType,
//								statePermitDueSMSNumber, ComplianceType.VENDOR, NotificationType.STATE_PERMIT_DUE,
//								propertiesConfig.getVendorStatePermitValid(), vendorDetail, mailDetails);
//					}
//					if (natnlPermitDueExpiryDay != 0 && !"NONE".equalsIgnoreCase(natnlPermitDueNotifiyType)) {
//						processNotification(branchId, currentDate, natnlPermitDueExpiryDay,
//								natnlPermitDueRepeatAlertsDay, natnlPermitDueEmailId, natnlPermitDueNotifiyType,
//								natnlPermitDueSMSNumber, ComplianceType.VENDOR, NotificationType.NATIONAL_PERMIT_DUE,
//								propertiesConfig.getVendorNationalPermitValid(), vendorDetail, mailDetails);
//					}
//					if (vehicelFitnessExpiryDay != 0 && !"NONE".equalsIgnoreCase(vehicelFitnessNotifiyType)) {
//						processNotification(branchId, currentDate, vehicelFitnessExpiryDay,
//								vehicelFitnessRepeatAlertsDay, vehicelFitnessEmailId, vehicelFitnessNotifiyType,
//								vehicelFitnessSMSNumber, ComplianceType.VENDOR, NotificationType.VEHICLE_FITNESS,
//								propertiesConfig.getVendorFitnessValid(), vendorDetail, mailDetails);
//					}
//				}
//			}
//
//		}
//	}

	@SuppressWarnings("unlikely-arg-type")
	private void processNotification(int branchId, Date currentDate, int[] expiryDays, int repeatAlert,
			String transportEmailId, String requestType, String smsNumber, ComplianceType compType,
			NotificationType notifyType, String sqlQuery, Object[] vendorDetail) {
		// log.info("processNotification==>" + sqlQuery);
		AuditType auditType;
		int primaryId = 0;
		String emailMsg = "";
		String smsMsg = "";
		String emailId = "";
		if (ComplianceType.DRIVER.equals(compType)) {
			auditType = AuditType.INTERNAL;
			smsMsg = propertiesConfig.getTransportDriverSms().replace("$complianceType$", "Driver")
					.replace("$SEPERATOR$", "\n");
			emailMsg = propertiesConfig.getTransportDriverEmail().replace("$complianceType$", "Driver")
					.replace("$SEPERATOR$", "\n");
		} else if (ComplianceType.VEHICLE.equals(compType)) {
			auditType = AuditType.INTERNAL;
			smsMsg = propertiesConfig.getTransportVehicleSms().replace("$complianceType$", "Vehicle")
					.replace("$SEPERATOR$", "\n");
			emailMsg = propertiesConfig.getTransportVehicleEmail().replace("$complianceType$", "Vehicle")
					.replace("$SEPERATOR$", "\n");
			;
		} else {
			auditType = AuditType.VENDOR;
			smsMsg = propertiesConfig.getTransportVendorSms().replace("$vendorName$", (String) vendorDetail[1])
					.replace("$complianceType$",
							("Driver".equalsIgnoreCase(notifyType.getType()) ? "Driver" : "Vehicle"))
					.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");
			emailMsg = propertiesConfig.getTransportVendorEmail().replace("$vendorName$", (String) vendorDetail[1])
					.replace("$complianceType$",
							("Driver".equalsIgnoreCase(notifyType.getType()) ? "Driver" : "Vehicle"))
					.replace("$logisticType$",
							"Driver".equalsIgnoreCase(notifyType.getType()) ? "Operation" : "Commercial")
					.replace("$complianceTypes$",
							"Driver".equalsIgnoreCase(notifyType.getType()) ? "driver/drivers" : "cab/cabs")
					.replace("$temp$", "Driver".equalsIgnoreCase(notifyType.getType()) ? "work" : "ply")
					.replace("$documentType$", notifyType.getDocumentType()).replace("$SEPERATOR$", "\n");

		}
		//emailMsg+= propertiesConfig.getTransportRegardsEmail();
		if (vendorDetail != null) {
			primaryId = (int) vendorDetail[0];
			Set<String> emailIds = getVendorContactDetailsByType(vendorDetail, ContactType.EMAIL);
			Set<String> mobileNumbers = getVendorContactDetailsByType(vendorDetail, ContactType.SMS);
			smsNumber += ",";
			smsNumber += String.join(",", mobileNumbers);
			emailId += String.join(",", emailIds);
		}
		boolean isTriggerNotification = false;
		String lasExecuteQuery = propertiesConfig.getLastExecutionVendor();
		List<Object[]> lastExecutionList = notificationService.getLastExecutionByNotifyType(lasExecuteQuery, branchId,
				auditType.name(), compType.name(), primaryId);
		if (!lastExecutionList.isEmpty())
			isTriggerNotification = CommonUtil.isTriggerNotification(lastExecutionList, repeatAlert, currentDate);
		else
			isTriggerNotification = true;

		File file;

		if (isTriggerNotification) {
			//Map<String, Object> notifyContent = getNotificationContentIfExist(sqlQuery, primaryId, currentDate,
					//expiryDays, compType, notifyType);
			Map<String, Object> notifyContent = new HashMap<String, Object>();
			//if (!notifyContent.isEmpty()) {
				notifyContent.put("smsContent", smsMsg);
				notifyContent.put("emailContent", emailMsg);
				List<Object[]> notifyDetails = notificationService.getDetailsBasedOnNotifyType(sqlQuery, primaryId, currentDate,
						expiryDays, compType);
				if(!notifyDetails.isEmpty()) {
					file = createExcelForEmail(sqlQuery, primaryId, currentDate, expiryDays, compType, notifyDetails);
					notifyContent.put("emailAttachment", file);
					triggerNotification(branchId, requestType, transportEmailId, emailId, smsNumber, auditType, compType,
							notifyContent, primaryId);
				}
		}

	}

	private File createExcelForEmail(String sqlQuery, int primaryId, Date currentDate, int[] expiryDays,
			ComplianceType complianceType, List<Object[]> notifyDetails) {

		String name = "os.name", filePath = "";

		boolean OsName = System.getProperty(name).startsWith("Windows");
		filePath = OsName ? propertiesConfig.getUploadDocsWindows() : propertiesConfig.getUploadDocsLinux();
		log.info("filePath" + filePath);

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Compliance Details");
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFCellStyle routetyle = workbook.createCellStyle();

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		Font headerRow = workbook.createFont();
		headerRow.setColor(IndexedColors.BLACK.getIndex());
		routetyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(174,182,191)));
		routetyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		routetyle.setFont(headerRow);

		style.setBorderTop((short) 6); // double lines border
		style.setBorderBottom((short) 1); // single line border

		XSSFFont compliantFont = workbook.createFont();
		compliantFont.setFontHeightInPoints((short) 12);
		compliantFont.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		style.setFillForegroundColor(new XSSFColor(new java.awt.Color(177, 162, 186)));
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(compliantFont);

		log.info("route Excel" + notifyDetails.size());

		if (ComplianceType.DRIVER.equals(complianceType))
			sheet = excelForDriver(workbook, sheet, routetyle, notifyDetails, expiryDays, currentDate, complianceType);
		else
			sheet = excelForVehicle(workbook, sheet, routetyle, notifyDetails, expiryDays, currentDate, complianceType);

		String fileName = "";
		fileName = "Compliance Document-"
				+ (ComplianceType.DRIVER.equals(complianceType) ? ComplianceType.DRIVER : ComplianceType.VEHICLE)
				+ dateFormat.format(new Date()) + "-" + Calendar.getInstance().get(Calendar.HOUR) + ""
				+ Calendar.getInstance().get(Calendar.MINUTE) + "" + Calendar.getInstance().get(Calendar.SECOND)
				+ ".xlsx";

		File file = new File(filePath, fileName);

		try {

			if (file.exists()) {
				file.delete();
			} else {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
		} catch (IOException e) {
			log.info(e.getMessage());
		}
		log.info("File Name : " + file);

		FileOutputStream stream;
		try {
			stream = new FileOutputStream(filePath.concat(fileName));
			workbook.write(stream);
			stream.flush();
			stream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return file;

	}

	private XSSFSheet excelForVehicle(XSSFWorkbook workbook, XSSFSheet sheet, XSSFCellStyle headerCellColorStyle,
			List<Object[]> notifyDetails, int[] expiryDays, Date currentDate, ComplianceType complianceType) {

		int rownum = 0;
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		XSSFCellStyle cellColorStyle = createCellBorder(workbook);
		// Vehicle Heading
		Row OutSiderow = sheet.createRow(rownum++);

		Cell cell0 = OutSiderow.createCell(0);
		cell0.setCellValue("Sr.No");
		cell0.setCellStyle(headerCellColorStyle);

		Cell cellVehicleNumber = OutSiderow.createCell(1);
		cellVehicleNumber.setCellValue("Vehicle Number");
		cellVehicleNumber.setCellStyle(headerCellColorStyle);

		CellStyle rightAligned = workbook.createCellStyle();
		rightAligned.setAlignment(CellStyle.ALIGN_LEFT);

		Cell cellVendorName = OutSiderow.createCell(2);
		cellVendorName.setCellValue("Vendor Name");
		cellVendorName.setCellStyle(rightAligned);
		cellVendorName.setCellStyle(headerCellColorStyle);

		Cell cellPollutionExpiry = OutSiderow.createCell(3);
		cellPollutionExpiry.setCellValue("Emission Certificate validity");
		cellPollutionExpiry.setCellStyle(headerCellColorStyle);

		Cell cellInsuranceExpiry = OutSiderow.createCell(4);
		cellInsuranceExpiry.setCellValue("Insurance Validity");
		cellInsuranceExpiry.setCellStyle(headerCellColorStyle);

		Cell cellTaxExpiry = OutSiderow.createCell(5);
		cellTaxExpiry.setCellValue("Tax Certificate validity");
		cellTaxExpiry.setCellStyle(headerCellColorStyle);

		Cell cellStateExpiry = OutSiderow.createCell(6);
		cellStateExpiry.setCellValue("Permit Validity");
		cellStateExpiry.setCellStyle(headerCellColorStyle);

		Cell cellNationalExpiry = OutSiderow.createCell(7);
		cellNationalExpiry.setCellValue("National Permit Expiry");
		cellNationalExpiry.setCellStyle(headerCellColorStyle);

		Cell cellVehicleFitnessExpiry = OutSiderow.createCell(8);
		cellVehicleFitnessExpiry.setCellValue("Fitness Certificate validity");
		cellVehicleFitnessExpiry.setCellStyle(headerCellColorStyle);

		int rowNum = 1;
		for (Object[] notifyDetail : notifyDetails) {

			Row insideRow = sheet.createRow(rownum++);

			/*XSSFCellStyle cellColorStyle = workbook.createCellStyle();
			cellColorStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			cellColorStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			cellColorStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
			Font cellFont = workbook.createFont();
			cellFont.setColor(IndexedColors.BLACK.getIndex());
			cellColorStyle.setFont(cellFont);*/

			Cell rowNo = insideRow.createCell(0);
			rowNo.setCellValue(rowNum++);
			rowNo.setCellStyle(cellColorStyle);
			
			Cell vehicleNumber = insideRow.createCell(1);
			if (notifyDetail[2] != null)
				vehicleNumber.setCellValue(notifyDetail[2].toString());
			vehicleNumber.setCellStyle(cellColorStyle);

			Cell vendorName = insideRow.createCell(2);
			String fName = notifyDetail[17] != null ? notifyDetail[17].toString() : "";
			vendorName.setCellValue(fName);
			vendorName.setCellStyle(cellColorStyle);

			Cell pollutionExpiry = insideRow.createCell(3);
			pollutionExpiry.setCellStyle(cellColorStyle);
			if (notifyDetail[3] != null) {
				pollutionExpiry.setCellValue(dateFormat.format(notifyDetail[3]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[3], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(),
						CommonUtil.NotificationType.POLLUTION_DUE.ordinal());
				setColorAccoringTocomplianceStatus(status, pollutionExpiry, workbook);
			} else {
				pollutionExpiry.setCellValue("NA");
			}

			Cell insuranceExpiry = insideRow.createCell(4);
			insuranceExpiry.setCellStyle(cellColorStyle);
			if (notifyDetail[4] != null) {
				insuranceExpiry.setCellValue(dateFormat.format(notifyDetail[4]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[4], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(),
						CommonUtil.NotificationType.INSURANCE_DUE.ordinal());
				setColorAccoringTocomplianceStatus(status, insuranceExpiry, workbook);
			} else {
				insuranceExpiry.setCellValue("NA");
			}

			Cell taxCertificateExpiry = insideRow.createCell(5);
			taxCertificateExpiry.setCellStyle(cellColorStyle);
			if (notifyDetail[5] != null) {
				taxCertificateExpiry.setCellValue(dateFormat.format(notifyDetail[5]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[5], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(), CommonUtil.NotificationType.TAX_DUE.ordinal());
				setColorAccoringTocomplianceStatus(status, taxCertificateExpiry, workbook);
			} else {
				taxCertificateExpiry.setCellValue("NA");
			}

			Cell statePermitExpire = insideRow.createCell(6);
			statePermitExpire.setCellStyle(cellColorStyle);
			if (notifyDetail[6] != null) {
				statePermitExpire.setCellValue(dateFormat.format(notifyDetail[6]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[6], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(),
						CommonUtil.NotificationType.STATE_PERMIT_DUE.ordinal());
				setColorAccoringTocomplianceStatus(status, statePermitExpire, workbook);
			} else {
				statePermitExpire.setCellValue("NA");
			}

			Cell nationalPermit = insideRow.createCell(7);
			nationalPermit.setCellStyle(cellColorStyle);
			if (notifyDetail[6] != null) {
				nationalPermit.setCellValue(dateFormat.format(notifyDetail[7]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[7], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(),
						CommonUtil.NotificationType.NATIONAL_PERMIT_DUE.ordinal());
				setColorAccoringTocomplianceStatus(status, nationalPermit, workbook);
			} else {
				nationalPermit.setCellValue("NA");
			}

			Cell vehicleMntPermit = insideRow.createCell(8);
			vehicleMntPermit.setCellStyle(cellColorStyle);
			if (notifyDetail[6] != null) {
				vehicleMntPermit.setCellValue(dateFormat.format(notifyDetail[8]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[8], currentDate,
						expiryDays, complianceType.VEHICLE.ordinal(),
						CommonUtil.NotificationType.VEHICLE_FITNESS.ordinal());
				setColorAccoringTocomplianceStatus(status, vehicleMntPermit, workbook);
			} else {
				vehicleMntPermit.setCellValue("NA");
			}

			for (int columnIndex = 0; columnIndex <= notifyDetail.length; columnIndex++) {
				sheet.autoSizeColumn(columnIndex);
			}
		}
		return sheet;

	}

	private XSSFSheet excelForDriver(XSSFWorkbook workbook, XSSFSheet sheet, XSSFCellStyle headerCellColorStyle,
			List<Object[]> notifyDetails, int[] expiryDays, Date currentDate, ComplianceType complianceType) {
		int rownum = 0;
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		XSSFCellStyle cellColorStyle = createCellBorder(workbook);
		// Driver Heading
		Row OutSiderow = sheet.createRow(rownum++);

		Cell cell0 = OutSiderow.createCell(0);
		cell0.setCellValue("Sr.No");
		cell0.setCellStyle(headerCellColorStyle);

		Cell cellDriverId = OutSiderow.createCell(1);
		cellDriverId.setCellValue("Driver Id");
		cellDriverId.setCellStyle(headerCellColorStyle);

		CellStyle rightAligned = workbook.createCellStyle();
		rightAligned.setAlignment(CellStyle.ALIGN_LEFT);

		Cell cellDriverName = OutSiderow.createCell(2);
		cellDriverName.setCellValue("Driver Name");
		cellDriverName.setCellStyle(rightAligned);
		cellDriverName.setCellStyle(headerCellColorStyle);
		
		Cell cellVendorName = OutSiderow.createCell(3);
		cellVendorName.setCellValue("Vendor Name");
		cellVendorName.setCellStyle(rightAligned);
		cellVendorName.setCellStyle(headerCellColorStyle);

		Cell routeNameHeading = OutSiderow.createCell(4);
		routeNameHeading.setCellValue("License Expiry validity");
		routeNameHeading.setCellStyle(headerCellColorStyle);

		Cell shftTime = OutSiderow.createCell(5);
		shftTime.setCellValue("Medical validity");
		shftTime.setCellStyle(headerCellColorStyle);

		Cell shftTimeVal = OutSiderow.createCell(6);
		shftTimeVal.setCellValue("Police Verification validity");
		shftTimeVal.setCellStyle(headerCellColorStyle);

		Cell emptyColumn = OutSiderow.createCell(7);
		emptyColumn.setCellValue("DD Training validity");
		emptyColumn.setCellStyle(headerCellColorStyle);

		int rowNum = 1;
		for (Object[] notifyDetail : notifyDetails) {

			Row insideRow = sheet.createRow(rownum++);

			/*XSSFCellStyle cellColorStyle = workbook.createCellStyle();
			cellColorStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			cellColorStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			cellColorStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
			Font cellFont = workbook.createFont();
			cellFont.setColor(IndexedColors.BLACK.getIndex());
			cellColorStyle.setFont(cellFont);*/
			

			Cell rowNo = insideRow.createCell(0);
			rowNo.setCellValue(rowNum++);
			rowNo.setCellStyle(cellColorStyle);
			
			Cell driverId = insideRow.createCell(1);			
			driverId.setCellStyle(cellColorStyle);
			if (notifyDetail[0] != null)
				driverId.setCellValue(notifyDetail[0].toString());

			Cell driverName = insideRow.createCell(2);
			String fName = notifyDetail[1] != null ? notifyDetail[1].toString() : "";
			String lName = notifyDetail[2] != null ? notifyDetail[2].toString() : "";
			driverName.setCellValue(fName + " " + lName);
			driverName.setCellStyle(cellColorStyle);
			
			Cell vendorName = insideRow.createCell(3);
			if (notifyDetail[17] != null)
				vendorName.setCellValue(notifyDetail[17].toString());
			vendorName.setCellStyle(cellColorStyle);


			Cell licenseExpiry = insideRow.createCell(4);
			licenseExpiry.setCellStyle(cellColorStyle);
			if (notifyDetail[3] != null) {
				licenseExpiry.setCellValue(dateFormat.format(notifyDetail[3]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[3], currentDate,
						expiryDays, complianceType.DRIVER.ordinal(),
						CommonUtil.NotificationType.LICENSE_EXPIRE.ordinal());
				setColorAccoringTocomplianceStatus(status, licenseExpiry, workbook);
			} else {
				licenseExpiry.setCellValue("NA");
			}

			Cell medicalExpiry = insideRow.createCell(5);
			medicalExpiry.setCellStyle(cellColorStyle);
			if (notifyDetail[4] != null) {
				medicalExpiry.setCellValue(dateFormat.format(notifyDetail[4]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[4], currentDate,
						expiryDays, complianceType.DRIVER.ordinal(),
						CommonUtil.NotificationType.MEDICAL_FITNESS.ordinal());
				setColorAccoringTocomplianceStatus(status, medicalExpiry, workbook);
			} else {
				XSSFCellStyle cellColorStyle1 = createCellBorder(workbook);
				medicalExpiry.setCellStyle(cellColorStyle1);
				medicalExpiry.setCellValue("NA");
			}

			Cell policeVerification = insideRow.createCell(6);
			policeVerification.setCellStyle(cellColorStyle);
			if (notifyDetail[5] != null) {
				policeVerification.setCellValue(dateFormat.format(notifyDetail[5]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[5], currentDate,
						expiryDays, complianceType.DRIVER.ordinal(),
						CommonUtil.NotificationType.POLICE_VERIFICATION.ordinal());
				setColorAccoringTocomplianceStatus(status, policeVerification, workbook);
			} else {
				XSSFCellStyle cellColorStyle1 = createCellBorder(workbook);
				policeVerification.setCellStyle(cellColorStyle1);
				policeVerification.setCellValue("NA");
			}

			Cell ddTraining = insideRow.createCell(7);
			ddTraining.setCellStyle(cellColorStyle);
			if (notifyDetail[6] != null) {
				ddTraining.setCellValue(dateFormat.format(notifyDetail[6]));
				int status = CommonUtil.getcomplianceStatusForNotifyType((Date) notifyDetail[6], currentDate,
						expiryDays, complianceType.DRIVER.ordinal(), CommonUtil.NotificationType.DD_TRAINING.ordinal());
				setColorAccoringTocomplianceStatus(status, ddTraining, workbook);
			} else {
				XSSFCellStyle cellColorStyle1 = createCellBorder(workbook);
				ddTraining.setCellStyle(cellColorStyle1);
				ddTraining.setCellValue("NA");
			}

			for (int columnIndex = 0; columnIndex <= notifyDetail.length; columnIndex++) {
				sheet.autoSizeColumn(columnIndex);
			}
		}
		return sheet;

	}
	
	private XSSFCellStyle createCellBorder(XSSFWorkbook workbook) {
		XSSFCellStyle cellColorStyle = workbook.createCellStyle();
		cellColorStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		cellColorStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		cellColorStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
		Font cellFont = workbook.createFont();
		cellFont.setColor(IndexedColors.BLACK.getIndex());
		cellColorStyle.setFont(cellFont);
		return cellColorStyle;
	}

	private Cell setColorAccoringTocomplianceStatus(int status, Cell cell, XSSFWorkbook workbook) {
		
		XSSFCellStyle cellColorStyle = 	createCellBorder(workbook);	

		if (status == complianceStatus.NOT_COMPLIANT.ordinal()) {
			cellColorStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(245,183,177)));
			cellColorStyle.setFillPattern(cellColorStyle.SOLID_FOREGROUND);
		} else if (status == complianceStatus.APPROACHING_TO_NONCOMPLIANT.ordinal()) {
			cellColorStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(212,230,241)));
			cellColorStyle.setFillPattern(cellColorStyle.SOLID_FOREGROUND);

		}
		cell.setCellStyle(cellColorStyle);
		return cell;
	}

	private Set<String> getVendorContactDetailsByType(Object[] vendorDetail, ContactType contactType) {
		Set<String> contact = new HashSet<String>();
		if (ContactType.SMS.equals(contactType)) {
			if (!StringUtils.isBlank((String) vendorDetail[3]))
				contact.add((String) vendorDetail[3]);
			if (!StringUtils.isBlank((String) vendorDetail[5]))
				contact.add((String) vendorDetail[5]);
			if (!StringUtils.isBlank((String) vendorDetail[6]))
				contact.add((String) vendorDetail[6]);
			if (!StringUtils.isBlank((String) vendorDetail[7]))
				contact.add((String) vendorDetail[7]);
			if (!StringUtils.isBlank((String) vendorDetail[8]))
				contact.add((String) vendorDetail[8]);
			if (!StringUtils.isBlank((String) vendorDetail[9]))
				contact.add((String) vendorDetail[9]);
		} else if (ContactType.EMAIL.equals(contactType)) {
			if (!StringUtils.isBlank((String) vendorDetail[4]))
				contact.add((String) vendorDetail[4]);
			if (!StringUtils.isBlank((String) vendorDetail[11]) && !contact.contains((String) vendorDetail[11]))
				contact.add((String) vendorDetail[11]);
			if (!StringUtils.isBlank((String) vendorDetail[12]) && !contact.contains((String) vendorDetail[12]))
				contact.add((String) vendorDetail[12]);
		}
		return contact;
	}

	private Map<String, Object> getNotificationContentIfExist(String sqlQuery, int primaryId, Date currentDate,
			int[] expiryDays, ComplianceType complianceType, NotificationType notifyType) {
		Map<String, Object> contentMap = new HashMap<String, Object>();

		List<Object[]> notifyDetails = notificationService.getDetailsBasedOnNotifyType(sqlQuery, primaryId, currentDate,
				expiryDays, complianceType);
		StringBuilder smsContent = new StringBuilder();
		int count = 1;
		if (!notifyDetails.isEmpty()) {
			if (ComplianceType.DRIVER.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType)
					&& notifyType.getType().equalsIgnoreCase(ComplianceType.DRIVER.name()))) {
				smsContent.append("Driver Id/Driver Name/License Expire/Medical Fitness Expire/Police Verification Expire/DDT Expire\n");
			} else if (ComplianceType.VEHICLE.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType)
					&& notifyType.getType().equalsIgnoreCase(ComplianceType.VEHICLE.name()))) {
				smsContent.append("VehicleId/VehicleNumber/Pollution Due Expire/Insurance Due Expire/Tax Certificate Due Expire/State Permit Due Expire/National Permit Due Expire/Vehicle Maintenance Expire\n");
			}
			for (Object[] notifyDetail : notifyDetails) {
				if (ComplianceType.DRIVER.equals(complianceType) || (ComplianceType.VENDOR.equals(complianceType)
						&& notifyType.getType().equalsIgnoreCase(ComplianceType.DRIVER.name()))) {
					int id = notifyDetail[0] != null ? (int) notifyDetail[0] : 0;
					smsContent.append(count + ") " + id + "/");
					String fName = notifyDetail[1] != null ? notifyDetail[1].toString() : "";
					String lName = notifyDetail[2] != null ? notifyDetail[2].toString() : "";
					smsContent.append(fName + " " + lName + "/");
					String licenseExpire = notifyDetail[3] != null ? notifyDetail[3].toString() : "";
					smsContent.append(licenseExpire + "/");
					String medicalFitnessExpire = notifyDetail[4] != null ? notifyDetail[4].toString() : "";
					smsContent.append(medicalFitnessExpire + "/");
					String policeVerificationExpire = notifyDetail[5] != null ? notifyDetail[5].toString() : "";
					smsContent.append(policeVerificationExpire + "/");
					String ddtExpire = notifyDetail[6] != null ? notifyDetail[6].toString() : "";
					smsContent.append(ddtExpire + "\n");

					// smsContent.append(expire+"\t\n");
				} else if (ComplianceType.VEHICLE.equals(complianceType)
						|| (ComplianceType.VENDOR.equals(complianceType)
								&& notifyType.getType().equalsIgnoreCase(ComplianceType.VEHICLE.name()))) {
					int id = notifyDetail[0] != null ? (int) notifyDetail[0] : 0;
					smsContent.append(count + ") " + id + "/");
					String vehicleNumber = notifyDetail[2] != null ? notifyDetail[2].toString() : "";
					smsContent.append(vehicleNumber + "/");
					String pollutionExpire = notifyDetail[3] != null ? notifyDetail[3].toString() : "";
					smsContent.append(pollutionExpire + "/");
					String insuranceExpire = notifyDetail[4] != null ? notifyDetail[4].toString() : "";
					smsContent.append(insuranceExpire + "/");
					String taxCertificateExpire = notifyDetail[5] != null ? notifyDetail[5].toString() : "";
					smsContent.append(taxCertificateExpire + "/");
					String stateCertificateExpire = notifyDetail[6] != null ? notifyDetail[6].toString() : "";
					smsContent.append(stateCertificateExpire + "/");
					String nationalCertificateExpire = notifyDetail[7] != null ? notifyDetail[7].toString() : "";
					smsContent.append(nationalCertificateExpire + "/");
					String vehicleCertificateExpire = notifyDetail[8] != null ? notifyDetail[8].toString() : "";
					smsContent.append(vehicleCertificateExpire + "\n");
				}
				count++;
			}
			smsContent.append("\n");
			contentMap.put("smsAttachment", smsContent.toString());
		}
		return contentMap;
	}

	private void triggerNotification(int branchId, String requestType, String transportEmailId, String emailId,
			String smsNumber, AuditType auditType, ComplianceType complianceType, Map<String, Object> notifyContent,
			int primaryId) {
		log.info(complianceType.name() + " : Trigger Notification started .....");
		boolean smsFlag = false;
		boolean emailFlag = false;
		if (requestType != null) {
			if ("BOTH".equalsIgnoreCase(requestType) || "SMS".equalsIgnoreCase(requestType)) {
				if (!StringUtils.isBlank(smsNumber)) {
					try {
						log.info("Sending SMS to " + smsNumber);
						String smsContent = notifyContent.get("smsContent") != null
								? (String) notifyContent.get("smsContent") + "\n"
								: "";
//						smsContent += notifyContent.get("smsAttachment") != null
//								? (String) notifyContent.get("smsAttachment")
//								: "";
						smsContent += propertiesConfig.getTransportRegardsSms().replace("$SEPERATOR$", "\n");
						final String smsContentFinal = smsContent;
						log.info("SMS Content " + smsContent);
						Thread thread1 = new Thread(new Runnable() {
							@Override
							public void run() {
								SMSNotificationUtil.sendSMS(propertiesConfig.getSmsUrl(), smsNumber.toString(),
										smsContentFinal);
							}
						});
						thread1.start();
						smsFlag = true;
						log.info("SMS sent successfully to " + smsNumber);
					} catch (Exception ex) {
						log.info("Exception while sending SMS : " + ex.getMessage());
					}
				}
			}
			if ("BOTH".equalsIgnoreCase(requestType) || "EMAIL".equalsIgnoreCase(requestType)) {
				try {
					Object mailDetails = notificationService.getEmailDetails(propertiesConfig.getEmailDetails(),
							branchId);

					emailId = !StringUtils.isBlank(emailId) ? emailId : propertiesConfig.getEmailDefaultMail();
					final String emailIdFinal = emailId;
					log.info("Sending Email to " + emailId + "Transport Team emails" + transportEmailId);
					String emailContent = notifyContent.get("emailContent") != null
							? (String) notifyContent.get("emailContent")
							: "";
					File emailAttachment = notifyContent.get("emailAttachment") != null
							? (File) notifyContent.get("emailAttachment")
							: null;
					log.info("Email Content " + emailAttachment);
					Thread thread1 = new Thread(new Runnable() {
						@Override
						public void run() {
							mailNotification.sendEmail(propertiesConfig.getEmailSubject(), emailContent,
									emailAttachment,
									propertiesConfig.getTransportRegardsEmail().replace("$SEPERATOR$", "\n"),
									emailIdFinal, mailDetails, transportEmailId);
						}
					});
					thread1.start();
//					log.info("Thread id "+thread1.getId());
					emailFlag = true;
					log.info("Email sent successfully to " + emailId);
				} catch (Exception ex) {
					ex.printStackTrace();
					log.info("Exception while sending Email : ");
				}
			}
			EFmFmComplianceAuditPO complianceAudit = new EFmFmComplianceAuditPO();
			complianceAudit.setAuditDate(new Date());
			complianceAudit.setAuditType(auditType.name());
			complianceAudit.setBranchId(branchId);
			complianceAudit.setComplianceType(complianceType.name());
			complianceAudit.setNotificationType("All");
			complianceAudit
					.setEmailFlag(emailFlag ? NotificationStatus.SENT.name() : NotificationStatus.NOTSENT.name());
			complianceAudit.setSmsFlag(smsFlag ? NotificationStatus.SENT.name() : NotificationStatus.NOTSENT.name());
			complianceAudit.setRequsetType(requestType);
			complianceAudit.setSmsSentTo(
					("BOTH".equalsIgnoreCase(requestType) || "SMS".equalsIgnoreCase(requestType) ? smsNumber : ""));
			complianceAudit.setEmailSentTo(
					("BOTH".equalsIgnoreCase(requestType) || "EMAIL".equalsIgnoreCase(requestType) ? emailId : ""));

			complianceAudit.setVendorId(primaryId);

			notificationService.save(complianceAudit);
		}
	}

}
