package com.efmfm.notification.service;


import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

public class EmailTest {

	public static void main(String[] args) {

		//				   Properties props = new Properties();
		//				   props.put("mail.smtp.host", "smtp.office365.com");
		//				   props.put("mail.smtp.auth", "TRUE");
		//				   props.put("mail.smtp.starttls.enable", "TRUE"); 
		//				  props.put("mail.smtp.port", 587);

		Email from = new Email("shell@efmfmtransport.com");
		//To email id
		String cc =null;
		String to = "atif.zafar@efmfm.com";

		Content content = new Content("text/html", "hello rajan sir please don't shout");
		Mail mail = Mail(from, "testing", to, cc, content);
		SendGrid sg = new SendGrid("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");
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println("- mail status code "+response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
			System.out.println("Sent mail successfully...TO : "+to);
			System.out.println(response.getStatusCode());
		} catch (Exception ex) {
             ex.printStackTrace();
		}
	}		
	public static Mail Mail(Email from, String subject, String to,String cc, Content contents)
	{
		Mail mail = new Mail();
		mail.setFrom(from);
		mail.setSubject(subject);

		String[] mailRecipient = to.split(",");
		Personalization personalization = new Personalization();
		for (int i = 0, size = mailRecipient.length; i < size; i++) {
			personalization.addTo(new Email(mailRecipient[i]));             
		}
		mail.addPersonalization(personalization);				    
		mail.addContent(contents);
		return mail; 
	}

}
