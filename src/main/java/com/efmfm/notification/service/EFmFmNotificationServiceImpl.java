package com.efmfm.notification.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.model.EmailServiceConfig;
import com.efmfm.notification.repository.EFmFmNotificationRepository;
import com.efmfm.notification.util.CommonUtil.ComplianceType;

@Service
public class EFmFmNotificationServiceImpl implements EFmFmNotificationService {

	@Autowired
	EFmFmNotificationRepository notificationRepo;

	@Override
	public List<Object[]> getAllBranchDetails(String query, List<String> combinedFacility) {
		return notificationRepo.getAllBranchDetails(query, combinedFacility);
	}

	@Override
	public List<Object[]> getLastExecutionByNotifyType(String lastExecutionQuery, int branchId, String auditType,
			String complianceType, int vendorId) {
		return notificationRepo.getLastExecutionByNotifyType(lastExecutionQuery, branchId, auditType,
				complianceType, vendorId);
	}

	@Override
	public List<Object[]> getDetailsBasedOnNotifyType(String sqlQuery, int branchId, Date startDate,int[] expiryDays, ComplianceType complianceType) {
		return notificationRepo.getDetailsBasedOnNotifyType(sqlQuery, branchId, startDate, expiryDays , complianceType);
	}

	@Override
	public void save(EFmFmComplianceAuditPO complianceAudit) {
		notificationRepo.save(complianceAudit);
		
	}

	@Override
	public List<Object[]> getVendorDetailsByBranchId(String sqlQuery, int branchId) {
		return notificationRepo.getVendorDetailsByBranchId(sqlQuery, branchId);
	}

	@Override
	public Object getEmailDetails(String query, int branchId) {
		return notificationRepo.getEmailDetails(query, branchId);
	}
	

}
