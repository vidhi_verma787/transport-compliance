package com.efmfm.notification.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.efmfm.notification.model.EFmFmComplianceAuditPO;
import com.efmfm.notification.model.EmailServiceConfig;
import com.efmfm.notification.util.CommonUtil;
import com.efmfm.notification.util.CommonUtil.ComplianceType;

@Repository
@SuppressWarnings("unchecked")
public class EFmFmNotificationRepositoryImpl implements EFmFmNotificationRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Object[]> getAllBranchDetails(String sqlQuery, List<String> combinedFacility) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchIds", combinedFacility);
		return query.getResultList();
	}

	@Override
	public List<Object[]> getLastExecutionByNotifyType(String sqlQuery, int branchId, String auditType,
			String complianceType, int vendorId) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchId", branchId)
				.setParameter("auditType", auditType).setParameter("complianceType", complianceType);
		if (vendorId != 0) {
			query.setParameter("vendorId", vendorId);
		}
		return query.getResultList();
	}

	@Override
	public List<Object[]> getDetailsBasedOnNotifyType(String sqlQuery, int primaryId, Date currentDate,
			int expiryDays[], ComplianceType complianceType) {
		Query query;
		Date endDate[] = new Date[10];
		if (ComplianceType.DRIVER.equals(complianceType)) {
			endDate[0] = CommonUtil.getAdditionDate(currentDate, expiryDays[0]);
			endDate[1] = CommonUtil.getAdditionDate(currentDate, expiryDays[1]);
			endDate[2] = CommonUtil.getAdditionDate(currentDate, expiryDays[2]);
			endDate[3] = CommonUtil.getAdditionDate(currentDate, expiryDays[3]);

			query = this.em.createNativeQuery(sqlQuery).setParameter("licenseEndDate", endDate[0])
					.setParameter("medicalEndDate", endDate[1]).setParameter("policeEndDate", endDate[2])
					.setParameter("ddtEndDate", endDate[3]);
		} else {
			endDate[0] = CommonUtil.getAdditionDate(currentDate, expiryDays[0]);
			endDate[1] = CommonUtil.getAdditionDate(currentDate, expiryDays[1]);
			endDate[2] = CommonUtil.getAdditionDate(currentDate, expiryDays[2]);
			endDate[3] = CommonUtil.getAdditionDate(currentDate, expiryDays[3]);
			endDate[4] = CommonUtil.getAdditionDate(currentDate, expiryDays[4]);
			endDate[5] = CommonUtil.getAdditionDate(currentDate, expiryDays[5]);

			query = this.em.createNativeQuery(sqlQuery).setParameter("PolutionValid", endDate[0])
					.setParameter("InsuranceValidDate", endDate[1]).setParameter("TaxCertificateValid", endDate[2])
					.setParameter("StatePermit", endDate[3]).setParameter("NationalPermit", endDate[4])
					.setParameter("VehicleFitNessDate", endDate[5]);

		}

		query.setParameter("vendorId", primaryId);
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void save(EFmFmComplianceAuditPO complianceAudit) {
		this.em.persist(complianceAudit);
	}

	@Override
	public List<Object[]> getVendorDetailsByBranchId(String sqlQuery, int branchId) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchId", branchId);
		return query.getResultList();
	}

	@Override
	public Object getEmailDetails(String sqlQuery, int branchId) {
		Query query = this.em.createNativeQuery(sqlQuery).setParameter("branchId", branchId);
		return query.getSingleResult();
	}

}
