package com.efmfm.notification.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "email_service_config")
@JsonIgnoreProperties(ignoreUnknown = true) 
public class EmailServiceConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "email_config_id", length = 10)
	private int emailConfigId;

	@Column(name = "send_grid_key", length = 100)
	private String sendGridKey;

	@Column(name = "email_service_provider", length = 50)
	private String emailServiceProvider;

	@Column(name = "email_username", length = 100)
	private String emailUserName;

	@Column(name = "email_password", length = 100)
	private String emailPassword;

	@Column(name = "email_host", length = 100)
	private String emailHost;

	@Column(name = "email_secureSSLORTLS", length = 100)
	private String emailSecureSSLORTLS;

	@Column(name = "email_auth",columnDefinition = "char(5) default 'true'")
	private String emailAuth;

	@Column(name = "email_port", columnDefinition = "Decimal(10) default '0'")
	private int emailPort;

	@Column(name = "branchId", columnDefinition = "int(10) default '0'")
	private int branchId;

	public int getEmailConfigId() {
		return emailConfigId;
	}

	public String getSendGridKey() {
		return sendGridKey;
	}

	public String getEmailServiceProvider() {
		return emailServiceProvider;
	}

	

	public String getEmailUserName() {
		return emailUserName;
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public String getEmailHost() {
		return emailHost;
	}

	public String getEmailSecureSSLORTLS() {
		return emailSecureSSLORTLS;
	}

	public String getEmailAuth() {
		return emailAuth;
	}

	public int getEmailPort() {
		return emailPort;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public void setEmailConfigId(int emailConfigId) {
		this.emailConfigId = emailConfigId;
	}

	public void setSendGridKey(String sendGridKey) {
		this.sendGridKey = sendGridKey;
	}

	public void setEmailServiceProvider(String emailServiceProvider) {
		this.emailServiceProvider = emailServiceProvider;
	}

	public void setEmailUserName(String emailUserName) {
		this.emailUserName = emailUserName;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}

	public void setEmailSecureSSLORTLS(String emailSecureSSLORTLS) {
		this.emailSecureSSLORTLS = emailSecureSSLORTLS;
	}

	public void setEmailAuth(String emailAuth) {
		this.emailAuth = emailAuth;
	}

	public void setEmailPort(int emailPort) {
		this.emailPort = emailPort;
	}

}
