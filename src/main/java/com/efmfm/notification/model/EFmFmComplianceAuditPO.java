package com.efmfm.notification.model;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="efmfm_compliance_audit")
public class EFmFmComplianceAuditPO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Audit_Id", length=10)
	private int auditId;
	
	@Column(name="Audit_Type", length=225)
	private String auditType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Audit_Date", length=30)
	private Date auditDate;
	
	@Column(name="Compliance_Type", length=225)
	private String complianceType;
	
	@Column(name="Notification_Type", length=225)
	private String notificationType;
	
	@Column(name="Request_Type", length=225)
	private String requsetType;
	
	@Column(name="Branch_Id", length=50)
	private int branchId;
	
	@Column(name="Sms_Flag", length=10)
	private String smsFlag;
	
	@Column(name="Email_Flag", length=10)
	private String emailFlag;
	
	@Column(name="Sms_Sent_To", length=500)
	private String smsSentTo;
	
	@Column(name="Email_Sent_To", length=500)
	private String emailSentTo;
	
	@Column(name="Vendor_Id", length=50)
	private int vendorId;
	
	public EFmFmComplianceAuditPO() {}

	public int getAuditId() {
		return auditId;
	}

	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}

	public String getAuditType() {
		return auditType;
	}

	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getComplianceType() {
		return complianceType;
	}

	public void setComplianceType(String complianceType) {
		this.complianceType = complianceType;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getRequsetType() {
		return requsetType;
	}

	public void setRequsetType(String requsetType) {
		this.requsetType = requsetType;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getSmsFlag() {
		return smsFlag;
	}

	public void setSmsFlag(String smsFlag) {
		this.smsFlag = smsFlag;
	}

	public String getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}

	public String getSmsSentTo() {
		return smsSentTo;
	}

	public void setSmsSentTo(String smsSentTo) {
		this.smsSentTo = smsSentTo;
	}

	public String getEmailSentTo() {
		return emailSentTo;
	}

	public void setEmailSentTo(String emailSentTo) {
		this.emailSentTo = emailSentTo;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	
}
