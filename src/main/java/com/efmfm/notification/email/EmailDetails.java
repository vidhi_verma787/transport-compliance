package com.efmfm.notification.email;

import java.io.File;

public class EmailDetails {

	private String userName;
	private String password;
	private String host;
	private String secureSSLORTLS;
	private String auth;
	private int port;
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private String subject;
	private String bodyContent;
	private String sendGridKey;
	private String sendEmailThrough;
	private File file;
	
	public String getUserName() {
		return userName;
	}
	public String getPassword() {
		return password;
	}
	public String getHost() {
		return host;
	}
	public String getSecureSSLORTLS() {
		return secureSSLORTLS;
	}
	public String getAuth() {
		return auth;
	}
	public int getPort() {
		return port;
	}
	public String getFrom() {
		return from;
	}
	public String getTo() {
		return to;
	}
	public String getCc() {
		return cc;
	}
	public String getBcc() {
		return bcc;
	}
	public String getSubject() {
		return subject;
	}
	public String getBodyContent() {
		return bodyContent;
	}
	public String getSendGridKey() {
		return sendGridKey;
	}
	public String getSendEmailThrough() {
		return sendEmailThrough;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public void setSecureSSLORTLS(String secureSSLORTLS) {
		this.secureSSLORTLS = secureSSLORTLS;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setBodyContent(String bodyContent) {
		this.bodyContent = bodyContent;
	}
	public void setSendGridKey(String sendGridKey) {
		this.sendGridKey = sendGridKey;
	}
	public void setSendEmailThrough(String sendEmailThrough) {
		this.sendEmailThrough = sendEmailThrough;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	
}
