package com.efmfm.notification.email;

import java.nio.file.Files;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GoDaddyEmailService  {
	private static Log log = LogFactory.getLog(GoDaddyEmailService.class);
	
	public static void sendMail(EmailDetails goDaddyDetails)
	{
		log.info("Sending mail through GoDaddy");
		final String username = goDaddyDetails.getUserName();
		final String password = goDaddyDetails.getPassword();
		Properties props = new Properties();
		props.put("mail.smtp.auth", goDaddyDetails.getAuth());
		props.put("mail.smtp.starttls.enable", goDaddyDetails.getSecureSSLORTLS()); 
		props.put("mail.smtp.host", goDaddyDetails.getHost());
		props.put("mail.smtp.port", goDaddyDetails.getPort());
		 // final String username ="genpact@efmfmtransport.com";// change accordingly
		 // final String password = "India$1299";// change accordingly
							
		
		 // // Properties props = new Properties();
		 // // props.put("mail.smtp.host", "smtp.office365.com");
		 // // props.put("mail.smtp.auth", "TRUE");
		 // // props.put("mail.smtp.starttls.enable", "TRUE"); 
		 // props.put("mail.smtp.port", 587);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(goDaddyDetails.getTo()));
			if(goDaddyDetails.getCc()!=null)
				message.setRecipients(Message.RecipientType.CC,
						InternetAddress.parse(goDaddyDetails.getCc()));
			message.setSubject(goDaddyDetails.getSubject());	
			
			// Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();
	         
	      // Now set the actual message
	         messageBodyPart.setText(goDaddyDetails.getBodyContent());

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);
	         
	         // Part two is attachment
			 byte[] fileContent = Files.readAllBytes(goDaddyDetails.getFile().toPath());
	         messageBodyPart = new MimeBodyPart();
	         String filename = goDaddyDetails.getFile().getName();
	         DataSource source = new ByteArrayDataSource(fileContent, "application/vnd.ms-excel");
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(filename);
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart);
			
			Transport.send(message);
			 log.info("Sent mail successfully...TO : "+goDaddyDetails.getTo()+goDaddyDetails.getCc()!=null?" CC : "+goDaddyDetails.getCc():"");

		} catch (Exception e) {
			log.error("exception occured while sending email: ",e);
		}
	}

}
