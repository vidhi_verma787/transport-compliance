package com.efmfm.notification.email;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.apache.commons.codec.binary.Base64;

public class SendGridEmailService {
	private static Log log = LogFactory.getLog(SendGridEmailService.class);
	public static void sendMail(EmailDetails sendGridDetails) throws IOException  {
		log.info("Sending mail through SendGrid");
		Email from = new Email(sendGridDetails.getFrom());
		//To email id
		String cc =null;
		String to = (sendGridDetails.getTo());
		if(null != sendGridDetails.getCc())
		cc = (sendGridDetails.getCc());
		Content content = new Content("text/html", sendGridDetails.getBodyContent().replace("\n","<br>"));
		Mail mail = Mail(from, sendGridDetails.getSubject(), to, cc, content, sendGridDetails.getFile());
		SendGrid sg = new SendGrid(sendGridDetails.getSendGridKey());
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			log.info(sendGridDetails.getSubject()+"- mail status code "+response.getStatusCode());
			log.info(response.getBody());
			log.info(response.getHeaders());
			log.info("Sent mail successfully...TO : "+sendGridDetails.getTo());
		} catch (Exception ex) {
			log.error("exception occured while sending email: ",ex);
			throw ex;
		}
	}

	public static Mail Mail(Email from, String subject, String to,String cc, Content content, File file)
	{
		Mail mail = new Mail();
		mail.setFrom(from);
		mail.setSubject(subject);
		
		String[] mailRecipient = to.split(",");
		Personalization personalization = new Personalization();
		for (int i = 0, size = mailRecipient.length; i < size; i++) {
		    personalization.addTo(new Email(mailRecipient[i]));             
		 }
		
		if(null != cc) {
		String[] transportEmailRecipient = cc.split(",");
		for (int i = 0, size = transportEmailRecipient.length; i < size; i++) {
		    personalization.addCc(new Email(transportEmailRecipient[i]));             
		 }
		}
		mail.addPersonalization(personalization);
		
		 Attachments attachments = new Attachments();
		    Base64 x = new Base64();
		    byte[] fileContent = null;
			try {
				fileContent = Files.readAllBytes(file.toPath());
				String imageDataString = x.encodeAsString(fileContent);
			    attachments.setContent(imageDataString);
			    attachments.setType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			    attachments.setFilename(file.getName());
			    attachments.setDisposition("attachment");
			    attachments.setContentId("ComplianceEmailAttachment");	
			    mail.addAttachments(attachments);
			} catch (IOException e) {
				e.printStackTrace();
			}
		    
		    
		mail.addContent(content);
		return mail; 
	}
}